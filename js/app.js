/**
 * Created by khantilabel.com on 17/06/2017.
 */


var elementTemplates = [
    {
        id: '0',
        tag: 'h1',
        content: 'My Story Header',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false,
    },
    {
        id: '1',
        tag: 'p',
        content: 'Type your text',
        content2: '',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false,
    },
    {
        id: '2',
        tag: 'img',
        content: '',
        content2: '',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false,
    },
    {
        id: '3',
        tag: 'yt',
        content: '',
        content2: '',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false
    },
    {
        id: '4',
        tag: 'reveal',
        content: 'img/bg1.jpg',
        content2: 'Caption 1',
        content3:'',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false
    },
    {
        id: '5',
        tag: 'reveal',
        content: 'img/bg2.jpg',
        content2: 'Caption 2',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false
    },
    {
        id: '6',
        tag: 'reveal',
        content: 'img/bg3.jpg',
        content2: 'Caption 3',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false
    },
    {
        id: '7',
        tag: 'h1',
        content: 'Text',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false
    },
    {
        id: '8',
        tag: 'img',
        content: 'img/smallimage1.jpg',
        attributes: {
            alignment: 'center',
            column: 'secondary'
        },
        showToolbar: false
    },
    {
        id: '9',
        tag: 'img',
        content: 'img/bg2.jpg',
        attributes: {
            alignment: 'center',
            column: 'secondary'
        },
        showToolbar: false
    },
    {
        id: '10',
        tag: 'img',
        content: 'img/smallimage3.jpg',
        attributes: {
            alignment: 'center',
            column: 'secondary'
        },
        showToolbar: false
    },
    {
        id: '11',
        tag: 'p2',
        content: '',
        content2: '',
        attributes: {
            alignment: 'center',
            column: 'main'
        },
        showToolbar: false,
    }
];

var data = {
    action:"",
    type:"",
    story_id:undefined,
    parent_id:"",
    navbar_link:"",
    return_url:"",
    backEndURL: 'http://khanti.pl/dev/eaglesb',
    onFinishPressed:false,
    globalID: 1,
    youtubeURL: 'https://www.youtube.com/watch?v=V8vejjVgIHg',
    sectionSelectionDialog: false,
    codeDialog: false,
    showMediaDialog: false,
    showCollectionDialog: false,
    openSettings:false,
    snackbarSaved:false,
    activeElement: null,
    themesAvailable:[
        'Dark','Light'
    ],
    e1:  { text: 'State 1', id:1 },
    listitems: [
        { text: 'State 1', id:1 },
        { text: 'State 2',  id:2 }

    ],
    sectionTypes: [
        {
            id: 'header_section',
            thumb:'img/section_header.png',
            title: 'Header Section',
            description: 'Header section cant be removed',
            isStatic: false,
            isImplemented: true,
            opacity:0.8,
            elements: [
                JSON.parse(JSON.stringify(elementTemplates[0])),
                JSON.parse(JSON.stringify(elementTemplates[1])),
                JSON.parse(JSON.stringify(elementTemplates[11]))
            ]
        },
        {
            id: 'media_section',
            thumb:'img/section_media.png',
            title: 'Media',
            description: 'Media section description...',
            isStatic: false,
            isImplemented: true,
            opacity:0,
            elements: []
        },
        {
            id: 'text_over_media',
            thumb:'img/section_text_over.png',
            title: 'Text Over Media',
            description: 'Text Over Media section description...',
            isStatic: false,
            isImplemented: true,
            opacity:0.8,
            elements: [
                JSON.parse(JSON.stringify(elementTemplates[7])),
                JSON.parse(JSON.stringify(elementTemplates[1])),
                JSON.parse(JSON.stringify(elementTemplates[11]))
            ]
        },
        {
            id: 'sinlge_column_text',
            thumb:'img/section_single.png',
            title: 'Single Column',
            description: 'Single Column section description...',
            isStatic: false,
            isImplemented: true,
            elements: [
                JSON.parse(JSON.stringify(elementTemplates[1]))
            ]
        },
        {
            id: 'two_column_text',
            title: 'Two Column',
            thumb:'img/section_two.png',
            description: 'Two Column Text section description...',
            isStatic: false,
            isImplemented: true,
            elements:[
                JSON.parse(JSON.stringify(elementTemplates[1])),
                JSON.parse(JSON.stringify(elementTemplates[8])), JSON.parse(JSON.stringify(elementTemplates[9])), JSON.parse(JSON.stringify(elementTemplates[10]))
            ]
        },
        {
            id: 'reveal_section',
            thumb:'img/section_reveal.png',
            title: 'Reveal Section',
            description: 'Reveal Section section description...',
            isStatic: false,
            isImplemented: true,
            elements: [
                JSON.parse(JSON.stringify(elementTemplates[4])), JSON.parse(JSON.stringify(elementTemplates[5])), JSON.parse(JSON.stringify(elementTemplates[6]))
            ]
        }

    ],
    story: {
        storyTitle: 'New Story',
        theme:"dark",
        author:"",
        sections: [
            {}
        ]
    },
    options: {
        targetBlank: 'true',
        toolbar: {buttons: ['bold', 'italic', 'underline', 'anchor']}
    },
    drawer: null,
    fixed: false,

    right: null,
    title: 'Piotr'
};

function CreateApp() {
 
    $(window).bind('beforeunload', function(e) {
        // Your code and validation
        
        if(app.onFinishPressed == false){
            if (confirm) {
                return "Are you sure?";
            } 
        }
        app.onFinishPressed  = false;
    });
     
Vue.component('page', {
    template: '#page',
    data: function () {
        return data
    }
});

Vue.component('side-bar', {

    template: '#side-bar',

    methods: {

        addSelectedSection: function (sectionType) {
            app.addSection(sectionType);
        }
    },
    data: function () {

        return data
    }
});
Vue.component('settings-window',{
    template:"#settings-window",
    props:['element'],
    methods:{


    },
    data: function () {
        return data
    }
});

Vue.component('add-media-window', {
    props: ['section'],
    template: '#add-media-window',


    methods: {
        doUpload: function () {


            var file = document.getElementById("myFileUploader").files[0];
            var formData = new FormData();

            formData.append('file', file);

            formData.append("api_key", "528214416419988");
            formData.append("upload_preset", "lgepeg1q");
            var xhr = new XMLHttpRequest();
            xhr.open('POST', "https://api.cloudinary.com/v1_1/eagle-review/image/upload", true);

            xhr.onload = function () {
                if (xhr.readyState === xhr.DONE) {
                    if (xhr.status === 200) {
                        var imageURL = JSON.parse(xhr.responseText).url;
                        data.showMediaDialog = false;

                        if (data.activeElement.$el.className.indexOf('header_section') != -1 || data.activeElement.$el.className.indexOf('media_section') != -1 || data.activeElement.$el.className.indexOf('text_over_media') != -1) {
                            data.activeElement.setBackgroundImage(imageURL);
                        }
                        else if (data.activeElement.$el.className.indexOf('reveal_section') != -1) {
                            var section = data.activeElement.section;
                            var newImage = app.copyJSON(elementTemplates[4]);
                            newImage.content = imageURL;
                            newImage.content2 = "New caption";
                            section.elements.push(newImage);
                            data.showCollectionDialog = true;
                        }
                        else {
                            data.activeElement.addNewElement('img', imageURL);
                        }


                        console.log(JSON.parse(xhr.responseText));
                    } else {
                        data.showMediaDialog = false;
                        console.log("error");
                    }
                }
            };
            xhr.send(formData);
        }
        ,
        saveYouTube: function () {
            //   this.data.youtubeURL

            var parsedURL = youtube_parser(data.youtubeURL);
            if (parsedURL == false) {
                alert("This is not a YouYube URL");

            }
            else {

                data.showMediaDialog = false;
                if (data.activeElement.$el.className.indexOf('header_section') != -1 || data.activeElement.$el.className.indexOf('text_over_media') != -1) {
                    data.activeElement.setBackgroundYouTube('https://www.youtube.com/embed/' + parsedURL + '?autoplay=1&loop=1&controls=0&autohide=1&rel=0&mute=1&playlist='+parsedURL);

                    data.activeElement.setBackgroundYouTubeThumb('https://img.youtube.com/vi/'+parsedURL +'/0.jpg');
                }
                else if (data.activeElement.$el.className.indexOf('media_section') != -1) {
                    data.activeElement.setBackgroundYouTube('https://www.youtube.com/embed/' + parsedURL +'?rel=0');
                    data.activeElement.setBackgroundYouTubeThumb('https://img.youtube.com/vi/'+parsedURL +'/0.jpg');
                }
                else {
                    data.activeElement.addNewElement('yt', 'https://www.youtube.com/embed/' + parsedURL+'?rel=0');
                    data.activeElement.setBackgroundYouTubeThumb('https://img.youtube.com/vi/'+parsedURL +'/0.jpg');
                }

            }
        }
    },
    data: function () {

        return {
            data: data
        }
    }
})

Vue.component('image-collection-window', {
        props: ['section', 'element'],
        template: '#image-collection-window',
        methods: {
            addNewImage: function () {
                data.showCollectionDialog = false;
                app.openMediaDialog(data.activeElement);

            }
        },
        data: function () {
            return data
        }
    }
)
Vue.component('section-full', {
    props: ['section'],
    template: '#section-full',
    methods: {
        changed: function (element, e) {

            element.content2 =   e.target.innerHTML.replace(/<\/?[^>]+(>|$)/g, "");

        },
        changed3: function (element,e) {

            element.content3 = e.target.innerHTML.replace(/<\/?[^>]+(>|$)/g, "");

        },
        changedFooter: function ( e) {

            data.story.author = e.target.innerHTML.replace(/<\/?[^>]+(>|$)/g, "");

        },
        openImageCollectionWindow: function () {
            data.activeElement = this.section;
            app.openImageCollectionWindow(this);

        },
        openBackgroundImageWindow: function () {
            app.openMediaDialog(this);
        },
        setBackgroundImage: function (imageURL) {
            this.section.backgroundImage = imageURL;
            this.section.backgroundYouTube = "";
        },
        setBackgroundYouTube: function (ytURL) {
            this.section.backgroundYouTube = ytURL;
            this.section.backgroundImage = "";
        },
        setBackgroundYouTubeThumb:function (url) {
            this.section.backgroundYouTubeThumb = url;
        },
        verticalAlignment: function (direction) {
            if (direction == "up") {

                if (this.section.vAlign == "center")
                    this.section.vAlign = "top";
                else if (this.section.vAlign == "bottom")
                    this.section.vAlign = "center";

            }
            else if (direction == "down") {
                if (this.section.vAlign == "center")
                    this.section.vAlign = "bottom";
                else if (this.section.vAlign == "top")
                    this.section.vAlign = "center";
            }

            window.ResizeApp();
        },
        horizontalAlignment: function (direction) {
            if (direction == "left") {

                if (this.section.hAlign == "center")
                    this.section.hAlign = "left";
                else if (this.section.hAlign == "right")
                    this.section.hAlign = "center";

            }
            else if (direction == "right") {
                if (this.section.hAlign == "center")
                    this.section.hAlign = "right";
                else if (this.section.hAlign == "left")
                    this.section.hAlign = "center";
            }
        },
        fontSize: function (direction) {
            if (direction == "up") {
                this.section.fontSize++;
                if (this.section.fontSize > 2) this.section.fontSize = 2;
            }
            else if (direction == "down") {
                this.section.fontSize--;
                if (this.section.fontSize < -2) this.section.fontSize = -2;
            }
            window.ResizeApp();
        }
    },
    computed: {
        headerClass: function () {
            return "header_h_" + this.section.hAlign + " " + "header_v_" + this.section.vAlign + " h-font-size-" + this.section.fontSize;

        },
        captionsClass: function () {
            return "caption_c_" + this.section.hAlign + " " + " c-font-size-" + this.section.fontSize;
        },
        sectionClass: function () {
            var theClass = this.section.sectionType.id;

            if (this.section.sectionType.id == 'text_over_media' && this.section.backgroundYouTube != "")
                theClass += " youtube-over";

            return theClass;
        }
    },
    data: function () {

        return data
    }
})

Vue.component('section-thumb', {
    props: ['section'],
    template: '#section-thumb',
    computed:{
        backgroundImage:function () {
          var imgURL = this.section.backgroundImage;
          if(this.section.sectionType.id == "reveal_section")
              imgURL = this.section.elements[0].content;
          else if(this.section.backgroundYouTube != "")
              imgURL = 'img/yt.jpg';
          return imgURL;
        },
        dynamicText:function () {
            var text = "";
            for(var i in this.section.elements){
                if(this.section.elements[i].tag == 'p' ||this.section.elements[i].tag == 'h1' ){
                    text = this.section.elements[i].content.replace(/<\/?[^>]+(>|$)/g, "");
                    break;
                }
                else   if(this.section.elements[i].tag == 'reveal'   ){
                    text = this.section.elements[i].content2.replace(/<\/?[^>]+(>|$)/g, " ");
                    break;
                }

            }
            return text;
        }
    },
    methods: {

        scrollTo: function () {
            app.scrollTo('section' + this.section.id);
        },
        removeSection: function () {

           app.removeSection(this.section);
        }
    }
});
Vue.component('image-collection-thumb', {
    props: ['element', 'section'],
    template: '#image-collection-thumb',
    methods: {
        scrollTo: function () {
            app.scrollTo('section' + this.section.id);
        },
        removeImage: function () {

            var i = 0;

            for (var e in this.section.elements) {
                var tempElement = this.section.elements[e];

                if (tempElement == this.element) {
                    this.section.elements.splice(i, 1);
                }
                i++;
            }
            i = 1;
            for (var s in this.section.elements) {
                this.section.elements[s].id = i++;


            }
        }
    }
});


Vue.component('html-element', {
    props: ['element', 'section'],
    components: {
        'medium-editor': vueMediumEditor.default
    },
    template: '#html-element',
    methods: {
        selected: function (e) {
            if (document.getSelection) {    // all browsers, except IE before version 9
                var sel = document.getSelection();
                // sel is a selectionRange object in all browsers except IE before version 9
                // the alert method displays the result of the toString method of the passed object
                console.log(sel);
            }
            else {

            }
        },
        changed: function (e) {

            this.element.content = e.target.innerHTML.replace(/<(?:.|\n)*?>/gm, '');

        },

        mouseOver: function () {
            this.element.showToolbar = true;
        },
        mouseOut: function () {
            this.element.showToolbar = false;
        },
        processEditOperation: function (operation) {
            console.log(operation);
            this.element.content = operation.event.srcElement.innerHTML
        }
    },
    data: function () {

        return {
            data: data
        }
    }
})


Vue.component('element-toolbar', {
    props: ['element', 'section'],
    template: '#element-toolbar',
    methods: {
        openMediaDialog: function () {
            app.openMediaDialog(this);

        },

        addNewElement: function (elementType, param1) {
            var newElement;

            if (elementType == "p") {
                newElement = app.copyJSON(elementTemplates[1]);

            }
            else if (elementType == "img") {
                newElement = app.copyJSON(elementTemplates[2]);
                newElement.content = param1;
            }
            else if (elementType == "yt") {
                newElement = app.copyJSON(elementTemplates[3]);
                newElement.content = param1;
            }

            newElement.attributes.column = this.element.attributes.column;

            this.section.elements.splice(this.element.id, 0, newElement);
            this.refreshID();
        },
        removeElement: function () {
            var i = 0;

            for (var e in this.section.elements) {
                var tempElement = this.section.elements[e];

                if (tempElement.id == this.element.id) {
                    this.section.elements.splice(i, 1);
                }
                i++;
            }
            this.refreshID();
           if(this.section.elements.length == 0) app.removeSection(this.section);

           if(this.section.sectionType.id == "two_column_text" ) {
               var bMergeColumns = false;

               if (this.section.elements.length == 1) {
                   bMergeColumns = true;
               } else {
                   var bFoundOtherColumn = false;
                   for (var i = 1; i < this.section.elements.length; i++) {

                       if (this.section.elements[i-1].attributes.column != this.section.elements[i].attributes.column) {
                           bFoundOtherColumn = true;
                       }
                   }

                   if(bFoundOtherColumn == false) bMergeColumns = true;
               }




               if(bMergeColumns == true){
                   this.section.sectionType = data.sectionTypes[3];
               }

           }
        },
        refreshID: function () {
            var i = 1;
            for (var e in this.section.elements) {
                this.section.elements[e].id = i++;

            }
        },
        moveUp:function () {
            var index = this.section.elements.indexOf(this.element);
            if(index != 0){

                var tempElement =   this.section.elements[index - 1];
                if(tempElement.attributes.column == this.element.attributes.column) {
                    this.section.elements[index - 1] = this.section.elements[index];
                    this.section.elements[index] = tempElement;
                }
            }

            this.refreshID();
        },
        moveDown:function () {

            var index = this.section.elements.indexOf(this.element);
            console.log(index +  " " +this.section.elements.length );
          if(index < (this.section.elements.length -1 ) ){

              var tempElement =   this.section.elements[index + 1];

              if(tempElement.attributes.column == this.element.attributes.column) {
                  this.section.elements[index + 1] = this.section.elements[index];
                  this.section.elements[index] = tempElement;
              }
          }

            this.refreshID();
        }
    }
});

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})






    var app = new Vue({
        el: '#app',
        data: data,
        created: function () {
            data.story.sections = [];
            data.story.author = "";
            //this.addSection(data.sectionTypes[0]);
            // this.loadStory();
            //  OnAppLoaded();
            // this.routeApp();
            this.onReady();
        },


        methods: {
            onReady: function () {
                data.story.sections = [];
                this.routeApp();
            },
            removeSection: function (section) {

                var i = 0;

                for (var s in data.story.sections) {
                    var tempSection = data.story.sections[s];

                    if (tempSection.id == section.id) {
                        data.story.sections.splice(i, 1);
                    }
                    i++;
                }
                i = 1;
                for (var s in data.story.sections) {
                    data.story.sections[s].id = i++;


                }
            },
            routeApp: function () {
                var params = getUrlParams(window.location.search);


                if (params.type != undefined)
                    data.type = params.type;
                if (params.return_url != undefined)
                    data.return_url = params.return_url;

                if (params.parent_id != undefined)
                    data.parent_id = params.parent_id;

          

                if (params.action != undefined) {

                    if (params.action == "new") {
                        this.createNewStory();

                    } else if (params.action == "edit") {
                        data.story_id = params.story_id;
                        this.loadStory();
                    }
                }


            },
            createNewStory: function () {
                data.story.sections = [];
                data.story.theme = "Dark";
                data.story.author = "";
                this.addSection(data.sectionTypes[0]);
                data.story_id = undefined;

            },

            saveNewStory:function(preview, bMobile){
                var dataToSend = {
                    'title': data.story.storyTitle,
                    'parent_id':data.parent_id,
                    'content': JSON.stringify(data.story),
                    'html': this.generateHTMLCode(),
                    'story_type': data.type
                };

                if(preview == true){
                    dataToSend = {
                        'title': data.story.storyTitle,
                        'parent_id':data.parent_id,
                        'content': JSON.stringify(data.story),
                        'preview_html': this.generateHTMLCode(),
                        'story_type': data.type
                    };
                }

                $.ajax({
                    url: "https://eagle-review.com/api/v1/stories",
                    type: 'POST',
                    data: JSON.stringify(dataToSend),
                    headers: {"Authorization": "Token token=" + window.token},
                    contentType: 'application/json'

                }).done(function (d) {
                    data.story_id = d.id;
                    console.log(data.story_id);

                    if(preview){
                        app.showPreview(bMobile)
                    }
                });
            },
            saveUpdatedStory:function(preview, bMobile) {
                var dataToSend = {

                    'content': JSON.stringify(data.story),
                    'html': this.generateHTMLCode(),

                };

                if(preview == true){
                     dataToSend = {


                        'preview_html': this.generateHTMLCode(),

                    };
                }

                $.ajax({
                    url: "https://eagle-review.com/api/v1/stories/" + data.story_id,
                    type: 'PUT',
                    data: JSON.stringify(dataToSend),
                    headers: {"Authorization": "Token token=" + window.token},
                    contentType: 'application/json'


                }).done(function (d) {
                  data.snackbarSaved = true;
                    console.log(d);
              //  });

                    if(preview){
                        app.showPreview(bMobile);
                    }
                });
            },
            saveStory: function () {

                if(data.story_id == undefined)//let's create a new stroy
                    this.saveNewStory();
                else
                    this.saveUpdatedStory();
                //    localStorage.setItem('storyBoardData', JSON.stringify(data.story));

            },
            loadStory: function () {
                $.ajax({
                    url: "https://eagle-review.com/api/v1/stories/" + data.story_id,
                    type: 'GET',

                    headers: {"Authorization": "Token token=" + window.token}

                }).done(function (d) {
                    data.type = d.story_type;

                    data.story = JSON.parse(d.content);
                    if(data.story.theme == undefined)
                        data.story.theme = "Dark";

                    if(data.story.author == undefined)
                        data.story.author = "";

                    for (var i in data.story.sections) {
                        var divs = 1;
                        var section = data.story.sections[i];
                        if(section.opacity == undefined  )
                            data.story.sections[i].opacity = 0.6;
                        }
                        if(section.content3 == undefined){
                            data.story.sections[i].content3 = "";

                        }

                        if(section.content != undefined)
                        section.content = section.content.replace(/<\/?[^>]+(>|$)/g, "");

                    if(section.content2 != undefined)
                        section.content2 = section.content2.replace(/<\/?[^>]+(>|$)/g, "");


                    if(section.content3 != undefined)
                        section.content3 = section.content3.replace(/<\/?[^>]+(>|$)/g, "");

                        if(section.backgroundYouTube != "" && section.backgroundYouTubeThumb==""){
                            var ytID = youtube_parser(section.backgroundYouTube);
                            section.backgroundYouTubeThumb = 'https://img.youtube.com/vi/'+ytID +'/0.jpg';
                        }
                    console.log(d);



                    window.ResizeApp();
                });

            },
            resetStory: function () {
                var txt;
                var r = confirm("Are you sure you want to clear the story without saving?");
                if (r == true) {
                    data.story = {
                        storyTitle: "New Story",
                        sections: [],
                        theme:"Dark",
                        author:""
                    };
                    this.addSection(data.sectionTypes[0]);
                } else {

                }


            },
            onFinish:function () {
                 app.saveStory();
                 data.onFinishPressed = true;
                 window.open(data.return_url,"_self");
            },
            openSectionSelectionDialog: function () {

            },
            scrollTo: function (sectionID) {
                var myDiv = document.getElementById(sectionID);
                var toolbarHeight = 60;
                doScrolling(myDiv.offsetTop - toolbarHeight, 200);
            },
            addSection: function (sectionType) {

                var sectionID = data.story.sections.length + 1;
                data.story.sections.push({
                    id: sectionID,
                    sectionType: sectionType,
                    elements: this.copyJSON(sectionType.elements),
                    backgroundImage: "",
                    backgroundYouTube: "",
                    backgroundYouTubeThumb:"",
                    opacity:sectionType.opacity,
                    vAlign: "center",
                    hAlign: "center",
                    fontSize: 0
                });
                var section = data.story.sections[data.story.sections.length - 1];

                if (sectionType.id == "header_section" || sectionType.id == "media_section") {
                    section.backgroundImage = "http://tremendouswallpapers.com/wp-content/uploads/2014/12/8589130409303-deer-wallpaper-hd.jpg";
                }

                setTimeout(function () {
                    var theID = 'section' + sectionID;
                    app.scrollTo(theID);
                    var firstP = document.querySelector('#' + theID + ' p');
                    if (firstP != null)
                        firstP.focus();

                }, 100);
                window.ResizeApp();
            },
            openImageCollectionWindow: function (element) {
                data.activeElement = element;
                data.showCollectionDialog = true;
            },
            openMediaDialog: function (element) {
                data.activeElement = element;
                data.showMediaDialog = true;


            },

            previewStoryBoard: function (bMobile) {
                if(data.story_id == undefined)//let's create a new stroy
                    this.saveNewStory(true, bMobile);
                else
                    this.saveUpdatedStory(true, bMobile);

            },
            showPreview: function (bMobile) {
                var windowReference;

                if(bMobile == true)
                    windowReference = window.open('','targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=320, height=480');
                else
                    windowReference = window.open();
                windowReference.location = '/stories/'+ data.story_id +'?preview=1';

            },
            generateHTMLCode: function (bMobile) {
                var bodyHTML = "";

                for (var i in data.story.sections) {
                    var divs = 1;
                    var section = data.story.sections[i];
                    if (section.sectionType == undefined) return;
                    var sectionHTML = '<div class="section-full ' + section.sectionType.id + '">';
                    var leftColumn, rightColumn;

                    if (section.sectionType.id == 'sinlge_column_text') {
                        sectionHTML += '<div class="content-column  text-element section-full-p ">';
                        divs++;
                    }
                    else if (section.sectionType.id == 'two_column_text') {
                        sectionHTML += '<div class="content-column grid  text-element section-full-p ">';
                        leftColumn = rightColumn = "";

                        divs++;
                    }
                    else if (section.sectionType.id == 'header_section') {

                        //  sectionHTML+= '<div class="background-image"></div> ';
                        if (section.backgroundImage != '') {
                            sectionHTML += '<div class="background-image"><img class="lazy" data-original="' + section.backgroundImage + '"/> <div class="background-overlay" style="opacity:'+((section.opacity/100)-0.2)+'"></div></div> ';
                        }
                        else if (section.backgroundYouTube != '') {
                            sectionHTML += '<div class="background-image"><iframe  width="100%" height="auto" class="yt-fullscreen"  src="' + section.backgroundYouTube + '"></iframe> <div class="background-overlay" style="opacity:'+section.opacity/100+'"></div></div> ';
                        }

                        sectionHTML += '<div class="header-wrapper ' + 'header_h_' + section.hAlign + ' header_v_' + section.vAlign + " h-font-size-" + section.fontSize + '">';
                        divs++;
                    }
                    else if (section.sectionType.id == 'text_over_media') {

                        //  sectionHTML+= '<div class="background-image"></div> ';
                        if (section.backgroundImage != '') {
                            sectionHTML += '<div class="background-image">  <div class="fixed-image" style="background-image:url(' + section.backgroundImage + ')"></div><div class="background-overlay" style="opacity:'+section.opacity/100+'"></div></div> ';
                        }
                        else if (section.backgroundYouTube != '') {

                            sectionHTML = '<div class="section-full ' + section.sectionType.id + ' youtube-over">';
                            sectionHTML += '<div class="background-image"><div class="embed-container"><iframe  width="100%" height="auto" class="desktop yt-fullscreen"  src="' + section.backgroundYouTube + '"></iframe><div class="background-image mobile" style="min-height: 400px"><img class="lazy" data-original="'+section.backgroundYouTubeThumb+'"/></div></div> <div class="background-overlay" style="opacity:'+section.opacity/100+'"></div></div> ';
                        }

                        sectionHTML += '<div class="header-wrapper ' + 'header_h_' + section.hAlign + ' header_v_' + section.vAlign + " h-font-size-" + section.fontSize + '">';
                        divs++;
                    }
                    else if (section.sectionType.id == 'media_section') {
                        if (section.backgroundImage != '') {
                            sectionHTML += '<div class="background-image"><img class="lazy" data-original="' + section.backgroundImage + '"/>  </div> ';
                        }
                        else if (section.backgroundYouTube != '') {
                            sectionHTML += '<div class="background-image"> <div   class="embed-container"><iframe  frameborder="0" allowfullscreen class="yt-fullscreen "  src="' + section.backgroundYouTube + '"></iframe></div></div> ';

                        }
                    }
                    else if (section.sectionType.id == 'reveal_section') {

                    }


                    for (var e in section.elements) {
                        var element = section.elements[e];
                        var elementHTML = '';

                        if (element.tag == 'h1') {
                            elementHTML = '<' + element.tag + '>' + element.content + '</' + element.tag + '>';
                        }
                        else if (element.tag == 'p') {
                            var pClass ="";//= ' text-element section-full-p ';
                            if(section.sectionType.id == "text_over_media" || section.sectionType.id == "header_section")
                                pClass = ' force-white';

                            // var content = element.content.replace(new RegExp('<p>', 'g'), '<p class="section-full-p">');
                            if (element.content.indexOf('<p>') != -1) {
                                var content = element.content.replace(new RegExp('<p>', 'g'), '<p class="'+pClass+'">');
                                elementHTML = content;
                            }
                            else
                                elementHTML = '<' + element.tag + ' class="'+pClass+'">' + element.content + '</' + element.tag + '>';
                        }
                        else if (element.tag == 'p2') {
                            var pClass = 'sub-header force-white';

                            elementHTML = '<p class="'+pClass+'">' + element.content + '</p>';
                        }
                        else if (element.tag == 'img') {
                            elementHTML = '<img class="lazy" data-original="' + element.content + '"/>';
                        }
                        else if (element.tag == 'yt') {
                            elementHTML = '<iframe  width="420" height="315" src="' + element.content + '"></iframe>';

                        }
                        else if (element.tag == 'reveal') {
                            elementHTML = '<div class="reveal_image"> <div class="reveal_bg">';
                            elementHTML += '<div class="reveal_img"  style="background-image:url(' + element.content + ')">';
                            if(element.content3 != undefined)
                            elementHTML += '<div class="header-wrapper ' + 'header_h_' + section.hAlign + ' header_v_' + section.vAlign + " h-font-size-" + section.fontSize + '"><h1>' + element.content3 + '</h1></div>';

                            elementHTML += '<div class="caption-wrapper"><p class="'+'caption_c_' + section.hAlign  +  ' c-font-size-'+ section.fontSize+'">' + element.content2 + '</p></div></div>';
                            elementHTML += '</div></div>';
                        }

                        if (section.sectionType.id == 'two_column_text') {

                            if (element.attributes.column == "main") {
                                leftColumn += elementHTML;
                            }
                            else if (element.attributes.column == "secondary") {
                                rightColumn += elementHTML;
                            }
                        }
                        else {
                            sectionHTML += elementHTML;
                        }
                    }



                    if (section.sectionType.id == 'two_column_text') {
                        sectionHTML += '<div class="left_column">' + leftColumn + '</div>';
                        sectionHTML += '<div class="right_column">' + rightColumn + '</div>';
                    }

                    for (var di = 0; di < divs; di++) {
                        if(di == 1){
                            if (section.sectionType.id == 'header_section'){
                                sectionHTML += '<footer>'+data.story.author + '</footer>';
                            }
                        }
                        sectionHTML += '</div>';
                    }
                    bodyHTML += sectionHTML;
                }

                var navBarHTML = '<div class="navbar"><a href="'+data.navbar_link+'"><img class="navbar-left-logo" src="img/logo.png"/></a><p class="nav-header">Story Board</p>  </div>'
                var styleCode = '<link href="http://khanti.pl/dev/eaglesb/css/storyboard.css" rel="stylesheet" type="text/css">';
               // var styleCode = '<link href="' + window.location.origin + window.location.pathname + 'css/storyboard.css" rel="stylesheet" type="text/css">';

                var htmlCode = '<!DOCTYPE html> <html lang="en"> <head> <meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>' + data.story.storyTitle + '</title>' + styleCode + '</head> <body class="viewer '+data.story.theme.toLowerCase()+'">' +navBarHTML+ bodyHTML + '<script type="text/javascript" src="http://khanti.pl/dev/eaglesb/js/jquery.min.js"></script><script type="text/javascript" src="http://khanti.pl/dev/eaglesb/js/jquery.lazyload.min.js"></script><script src="http://khanti.pl/dev/eaglesb/js/storyboard.js"></script></body> </html>';
             //   var htmlCode = '<!DOCTYPE html> <html lang="en"> <head> <meta charset="UTF-8"> <title>' + data.story.storyTitle + '</title>' + styleCode + '</head> <body class="viewer">' + bodyHTML + '<script type="text/javascript" src="' + window.location.origin + window.location.pathname + 'js/jquery.min.js"></script><script src="' + window.location.origin + window.location.pathname + 'js/storyboard.js"></script></body> </html>';



                return htmlCode;
            },

            copyJSON: function (jsonValue) {
                return JSON.parse(JSON.stringify(jsonValue));
            }
        }

    });
}
var entityMap = {
    "&": "_amp_",
    "<": "_lt_",
    ">": "_gt_",
    '"': '_quot_',
    "'": '_39_',
    "/": '_x2F_'
}

function escapeHtml(string) {

    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}

function doScrolling(elementY, duration) {
    var startingY = window.pageYOffset
    var diff = elementY - startingY
    var start

    // Bootstrap our animation - it will get called right before next frame shall be rendered.
    window.requestAnimationFrame(function step(timestamp) {
        if (!start) start = timestamp
        // Elapsed miliseconds since start of scrolling.
        var time = timestamp - start
        // Get percent of completion in range [0, 1].
        var percent = Math.min(time / duration, 1)

        window.scrollTo(0, startingY + diff * percent)

        // Proceed with animation as long as we wanted it to.
        if (time < duration) {
            window.requestAnimationFrame(step)
        }
    })
}

function youtube_parser(url) {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match && match[7].length == 11) ? match[7] : false;
}

function getUrlParams(url) {
    var queryString = url.split("?")[1];
    var keyValuePairs = queryString.split("&");
    var keyValue, params = {};
    keyValuePairs.forEach(function(pair) {
        keyValue = pair.split("=");
        params[keyValue[0]] = decodeURIComponent(keyValue[1]).replace("+", " ");
    });
    return params
}
/*


    app.onReady();
};*/

CreateApp();

