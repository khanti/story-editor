/**
 * Created by piotrostiak on 18.07.2017.
 */

window.ResizeApp = function (){

        if($(window).width() > 600) {

            $('.reveal_image').css('background-position', 'center center' );
        }

        $('.text_over_media, .header_section, .reveal_image').each(function(index){

                var wrapper = $(this).find('.header-wrapper');
                if(wrapper.hasClass('header_v_center')) {
                    var wrapperH = $(this).height() / 2 - wrapper.height() / 2;

                    wrapper.css({'top': wrapperH + 'px'})
                }else if(wrapper.hasClass('header_v_top')) {
                    wrapper.css({'top':  '0'})
                }
                else  {
                    var wrapperH = $(this).height()  - wrapper.height();
                    wrapper.css({'top': wrapperH + 'px'})

                }
                // header_v_top
            }
        ) ;


}
$(function () {
    var previousCaption = undefined;
    $(window).on('resize', function () {
        window.ResizeApp();
    });
    $(window).on('scroll', function (){


    });

    $.fn.isOnScreen = function(index){

        var win = $(window);

        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if(index == 0)
        return ((  (viewport.bottom - win.height()/2) > bounds.top && viewport.top < bounds.bottom));
        else
        return (( (viewport.bottom) > bounds.top && viewport.top < bounds.bottom && viewport.bottom < bounds.bottom));

    };

    $(function() {
        $("img.lazy").lazyload({
            threshold : 200,
            effect : "fadeIn"
        });
    });

    setTimeout( function(){ window.ResizeApp();}, 500);
    setTimeout( function(){ window.ResizeApp();}, 1500);
})